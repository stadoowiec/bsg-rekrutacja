//@ts-check
import axios from 'axios'
const api_url = 'https://thebetter.bsgroup.eu/'

axios.interceptors.response.use((response) => {
    return response
}, (error) => {
    window.location.replace('/LogIn')
})

export const signIn = async (username, password) => {
    try {
        const data = username && password ? { username, password } : {}
        const response = await axios({
            method: 'POST',
            url: api_url + 'Authorization/SignIn',
            headers: { 'content-type': 'application/json' },
            data
        })

        return response
    } catch (error) {
        return null
    }
}

export const getMediaList = async (token, pageNumber) => {
    try {
        const response = await axios({
            method: 'POST',
            url: api_url + 'Media/GetMediaList',
            headers: { 'content-type': 'application/json', 'Authorization': 'Bearer ' + token },
            data: {
                "MediaListId": 2,
                "IncludeCategories": false,
                "IncludeImages": true,
                "IncludeMedia": false,
                "PageNumber": pageNumber,
                "PageSize": 15
            }
        })

        return response
    } catch (error) {
        return null
    }
}

export const getMediaPlayInfo = async (filmId, token, streamType) => {
    try {
        const response = await axios({
            method: 'POST',
            url: api_url + 'Media/GetMediaPlayInfo',
            headers: { 'content-type': 'application/json', 'Authorization': 'Bearer ' + token },
            data: {
                MediaId: +filmId,
                StreamType: streamType,
            }
        })

        return response
    } catch (error) {
        return null
    }
}