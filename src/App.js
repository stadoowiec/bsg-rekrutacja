//@ts-check
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import FilmsList from './components/filmsList'
import Film from './components/film'
import SplashScreen from './components/splashScreen'
import Login from './components/login'

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path='/film/:filmId'>
          <Film />
        </Route>
        <Route path='/FilmsList'>
          <FilmsList />
        </Route>
        <Route path='/LogIn'>
          <Login />
        </Route>
        <Route path='/'>
          <SplashScreen />
        </Route>
      </Switch>
    </BrowserRouter>
  )
}

export default App