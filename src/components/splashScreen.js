import { useEffect } from 'react'
import bird from '../assets/bird.svg'
import styles from './splashScreen.module.css'
import { useHistory } from 'react-router-dom'

const SplashScreen = () => {
    const history = useHistory()

    useEffect(() => {
        setTimeout(() => {
            history.push('/Login')
        }, 4000)
    }, [history])

    return (
        <div className={styles.backdrop}>
            <img
                src={bird}
                alt='Bird' />
        </div>
    )
}

export default SplashScreen