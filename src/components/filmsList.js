//@ts-check
import React, { useEffect, useState, useContext } from 'react'
import { Link } from 'react-router-dom'

import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'

import styles from './filmsList.module.css'
import bird from '../assets/bold-bird.svg'

import { getMediaList } from '../services/api-service'
import Context from '../store/context'
import Pagination from './pagination'

const FilmsList = () => {
    const [list, setList] = useState([])
    const [pagesCount, setPagesCount] = useState(0)
    const [currentPage, setCurrentPage] = useState(1)

    const context = useContext(Context)

    useEffect(() => {
        if (context.token) {
            (async () => {
                const mediaList = await getMediaList(context.token, currentPage)
                setList(mediaList.data.Entities)
                const filmsQty = mediaList.data.TotalCount
                const pagesQty = Math.ceil(filmsQty / 15)
                setPagesCount(pagesQty)
            })()
        }
    }, [context.token, currentPage])

    const navBar = (
        <Navbar className={styles.navbar} variant='dark' fixed='top'>
            <Container className={styles.container}>
                <Navbar.Brand href='#home'>
                    <img
                        alt=''
                        src={bird}
                        width='40'
                        height='40'
                        className='d-inline-block align-center'
                    />
                </Navbar.Brand>
                {context.username ? `Hello ${context.username}!` : 'You are currently using a trial version'}
            </Container>
        </Navbar>
    )

    let finalList = list && list.map(list => {
        return (
            <div className={styles.tile} key={list.Id}>
                <Link to={`/film/${list.Id}`}>
                    <h2 className={styles.title}>{list.Title}</h2>
                    <div className={styles['image-wrapper']}>
                        {list.Images.filter(image => image.ImageTypeCode === 'FRAME')
                            .map(i => <img
                                alt={list.Title}
                                key={i.Id}
                                src={i.Url} />)}
                    </div>
                </Link>
            </div>
        )
    })

    return (
        <div className={styles.list}>
            {navBar}
            {finalList}
            <Pagination onClickHandler={setCurrentPage} allPagesCount={pagesCount} currentPage={currentPage} />
        </div>
    )
}

export default FilmsList