import { useParams, useHistory } from 'react-router-dom'
import { useContext, useEffect, useState } from 'react'

import ReactPlayer from 'react-player'
import Alert from 'react-bootstrap/Alert'

import { getMediaPlayInfo } from '../services/api-service'
import styles from './film.module.css'
import Context from '../store/context'

const Film = () => {
    const history = useHistory()

    const [filmUrl, setFilmUrl] = useState('')
    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [message, setMessage] = useState('Loading...')

    const context = useContext(Context)

    let { filmId } = useParams()

    useEffect(() => {
        (async () => {
            const streamType = context.username ? 'MAIN' : 'TRIAL'
            const film = await getMediaPlayInfo(filmId, context.token, streamType)

            if (!film?.data?.ContentUrl) {
                setMessage('Something went wrong')
                return
            }
            setFilmUrl(film.data.ContentUrl)
            setTitle(film.data.Title)
            setDescription(film.data.Description)

        })()
    }, [context.username, filmId, context.token])

    const goBackHandler = () => history.goBack()

    const player = (
        <ReactPlayer
            url={filmUrl}
            width='100%'
            height='100%'
            playing={true}
            controls={true} />
    )

    const goBack = (
        <button className={styles.button} type='button' onClick={goBackHandler}>
            &lt;&lt;&lt; Go back
        </button>
    )

    const details = (
        <Alert variant='dark'>
            <h2>{title}</h2>
            <p>{description}</p>
        </Alert>
    )

    const error = (
        <Alert variant='danger'>{message}</Alert>
    )

    const display = filmUrl ? <>{player} {details} </> : error

    return (
        <div className={styles.wrapper}>
            {display}
            {goBack}
        </div>
    )
}

export default Film