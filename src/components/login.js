//@ts-check
import { useContext, useEffect, useState } from 'react'
import styles from './login.module.css'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Context from '../store/context'
import { useHistory } from 'react-router-dom'
import Alert from 'react-bootstrap/Alert'

import { signIn } from '../services/api-service'

const Login = () => {
    const context = useContext(Context)
    const history = useHistory()

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState(false)

    useEffect(() => {
        if (!error && context.token) {
            history.push('/filmsList')
        }
    }, [error, context.token, context.username, history])

    const signInHelper = async (username = '', password = '') => {
        setError(false)
        const response = await signIn(username, password)
        const token = response?.data?.AuthorizationToken?.Token
        token ? context.addUserInfo(token, username) : setError(true)
    }

    const onSignIn = async e => {
        e.preventDefault()
        await signInHelper(username, password)
    }

    const skipLogInHandler = async () => await signInHelper()

    return (
        <div className={styles['login-panel']}>
            <h1>Log in</h1>
            <Form onSubmit={onSignIn}>
                <Form.Group className='mb-3' controlId='formBasicEmail'>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                        type='email'
                        placeholder='Enter email'
                        value={username}
                        onChange={(event) => setUsername(event.target.value)}
                    />
                </Form.Group>

                <Form.Group className='mb-3' controlId='formBasicPassword'>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type='password'
                        placeholder='Password'
                        value={password}
                        onChange={(event) => setPassword(event.target.value)}
                    />
                </Form.Group>
                <div className={styles.submit}>
                    <Button variant='outline-danger' type='submit'>
                        Submit
                    </Button>{' '}
                    <Button variant='link' type='button' onClick={skipLogInHandler}>
                        Skip logging in
                    </Button>
                    <div className={styles.error}>
                        {error ? <Alert variant='danger'>
                            Wrong username or password!
                        </Alert> : null}
                    </div>
                </div>
            </Form>
        </div>
    )
}

export default Login