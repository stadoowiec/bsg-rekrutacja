import Pagination from 'react-bootstrap/Pagination'
import './pagination.css'

const PaginationComponent = ({ allPagesCount, currentPage, onClickHandler }) => {
    let items = []
    for (let number = 1; number <= allPagesCount; number++) {
        items.push(
            <Pagination.Item
                onClick={() => onClickHandler(number)}
                key={number}
                active={number === currentPage}>
                {number}
            </Pagination.Item>
        )
    }

    return <Pagination>{items}</Pagination>
}

export default PaginationComponent