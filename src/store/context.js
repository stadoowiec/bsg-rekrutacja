import React from 'react'

const Context = React.createContext({
    token: null,
    username: '',
    addUserInfo: (token, username) => { }
})

export default Context