import { useReducer } from 'react'
import Context from './context'

const defaultContext = {
    token: null,
    username: '',
    addUserInfo: (token, username) => { }
}

const reducer = (state, action) => {
    if (action.type === 'ADD_USER_INFO') {
        return {
            ...state,
            token: action.token,
            username: action.username,
        }
    }
}

const Provider = props => {
    const [ctx, action] = useReducer(reducer, defaultContext)

    const addUserInfo = (token, username = null) => {
        action({
            type: 'ADD_USER_INFO',
            token,
            username
        })
    }

    const context = {
        token: ctx.token,
        username: ctx.username,
        addUserInfo: addUserInfo
    }

    return <Context.Provider value={context}>{props.children}</Context.Provider>
}

export default Provider